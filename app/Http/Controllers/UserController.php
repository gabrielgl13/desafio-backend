<?php

namespace App\Http\Controllers;

use App\Http\Requests\LoginRequest;
use App\Http\Requests\RegisterRequest;
use App\User;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Auth;
use Illuminate\Support\Facades\DB;

class UserController extends Controller
{
    /**
     * Autentica um usuário
     * 
     */
    public function login(LoginRequest $request) 
    {
        // Faz a autenticação com as credenciais informadas
        if (Auth::attempt(['email' => $request->email, 'password' => $request->password])) {
            // Cria o token
            $tokenResult = Auth::user()->createToken('Personal Access Token');
            
            // Retorna o token
            return response()->json([
                'access_token' => $tokenResult->accessToken,
                'token_type' => 'Bearer',
            ], 201);
        }

        // Retorna não autorizado
        return response()->json([
            'message' => 'Unauthorized'
        ], 401);
    }

    /**
     * Registra um novo usuário
     * 
     */
    public function register(RegisterRequest $request)
    {
        // Inicia transação
        DB::beginTransaction();
        try {
            // Cadastra um novo usuário
            $user = User::create($request->only(['name', 'email', 'password']));
            // Autentica ele
            Auth::login($user);
             // Cria o token
             $tokenResult = Auth::user()->createToken('Personal Access Token');
            // Finaliza transação e retorna
            DB::commit();
             // Retorna o token
             return response()->json([
                 'access_token' => $tokenResult->accessToken,
                 'token_type' => 'Bearer',
             ], 201);
        } catch (\Throwable $e) {
            // Retorna transação
            DB::rollBack();
            return response()->json(['failed' => $e->getMessage()], 401);
        }
    }
}
