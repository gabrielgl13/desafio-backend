<?php

namespace App\Http\Controllers;

use App\Data\Ferramenta\Ferramenta;
use App\Data\Ferramenta\Resources;
use App\Data\FerramentaTag\FerramentaTag;
use App\Data\Tag\Tag;
use App\Http\Requests\FerramentaRequest;
use Illuminate\Support\Facades\DB;

class FerramentaController extends Controller
{
    /**
     * Cadastra uma ferramenta
     * 
     */
    public function store(FerramentaRequest $request)
    {
        // Inicia transação
        DB::beginTransaction();
        try {
            // Cria uma nova ferramenta
            $ferramenta = Ferramenta::create($request->only(['title', 'link', 'description']));
                        
            // Cria as tags informadas
            foreach ($request->tags as $tag) {
                // Retorna a tag ou cria uma se ela não existir
                $tagCriada = Tag::firstOrCreate(['name' => $tag]);
                // Cria um novo registro para a relação N:N
                $ferramenta->tags()->attach($tagCriada);
            }
            // Finaliza transação
            DB::commit();
            return response()->json(['success' => $ferramenta], 201);
        } catch (\Throwable $e) {
            // Volta a transação
            DB::rollBack();
            return response()->json(['failed' => $e->getMessage()], 401);
        }
    }

    /**
     * Lista todas as ferramentas com suas respectivas tags
     * 
     */
    public function list()
    {
        $ferramentas = Ferramenta::with('tags')->get();

        $data = $ferramentas->map(function ($ferramenta) {
            return new Resources($ferramenta);
        });

        return response()->json($data, 201);
    }

    /**
     * Lista as ferramentas de acordo com a tag
     * 
     */
    public function listByTag($name)
    {
        // Obtém a tag com o nome informado
        $tag = Tag::where('name', $name)->firstOrFail();
        // Obtém as ferramentas
        $ferramentas = $tag->ferramentas()->get();
        
        $data = $ferramentas->map(function ($ferramenta) {
            return new Resources($ferramenta);
        });

        return response()->json($data, 201);
    }

    /**
     * Deleta uma ferramenta
     * 
     */
    public function destroy(Ferramenta $ferramenta)
    {
        DB::beginTransaction();

        try {
            // Obtém as tags da ferramenta
            $tags = $ferramenta->tags()->get();
            // Exclui a ferramenta e suas relações
            $ferramenta->tags()->detach($tags);
            $ferramenta->delete();
            // Finaliza a transação
            DB::commit();
            return response()->json(['message' => 'deletado com sucesso.'], 204);
        } catch (\Throwable $e) {
            // Retorna a transação
            DB::rollback();
            return response()->json(['message' => 'falha ao tentar remover o item.'], 401);
        }
    }
}
