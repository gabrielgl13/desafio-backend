<?php

namespace App\Data\FerramentaTag;

use Illuminate\Database\Eloquent\Model;

class FerramentaTag extends Model
{
    protected $table = 'ferramentas_tags';

    protected $fillable = [
        'ferramenta_id',
        'tag_id'
    ];
}

// End of File
