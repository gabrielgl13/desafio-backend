<?php

namespace App\Data\Ferramenta;

use Illuminate\Database\Eloquent\Model;

class Ferramenta extends Model
{
    use Relationship;

    protected $table = 'ferramentas';

    protected $fillable = [
        'title',
        'link',
        'description',
        'tag_id'
    ];
}

// End of File

