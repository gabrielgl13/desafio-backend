<?php

namespace App\Data\Ferramenta;

use App\Data\Tag\Resources as AppResources;
use Illuminate\Http\Resources\Json\JsonResource;

class Resources extends JsonResource
{
    /**
     * Transform the resource into an array.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return array
     */
    public function toArray($request)
    {
        return [
            'id' => $this->id,
            'title' => $this->title,
            'description' => $this->description,
            'link' => $this->link,
            'tags' => $this->tags->map(function($tag) {
                        return $tag->name;
                    })->toArray()
        ];
    }
}
