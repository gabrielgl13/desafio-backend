<?php

namespace App\Data\Ferramenta;

trait Relationship
{
    /**
     * Uma ferramenta tem uma única tag
     * Uma tag ser contida em várias ferramentas
     * 
     */
    public function tags()
    {
        return $this->belongsToMany('App\Data\Tag\Tag', 'ferramentas_tags', 'ferramenta_id', 'tag_id')
                ->withTimestamps();
    }
}

// End of File
