<?php

namespace App\Data\Tag;

use Illuminate\Database\Eloquent\Model;

class Tag extends Model
{
    use Relationship;
    
    protected $table = 'tags';

    protected $fillable = [
        'name'
    ];
}
