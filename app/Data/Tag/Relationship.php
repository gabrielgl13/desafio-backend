<?php

namespace App\Data\Tag;

trait Relationship
{
    /**
     * Uma tag pode ser contida por várias ferramentas
     * Uma ferramenta terá uma única tag
     * 
     */
    public function ferramentas()
    {
        return $this->belongsToMany('App\Data\Ferramenta\Ferramenta', 'ferramentas_tags', 'tag_id', 'ferramenta_id')
            ->withTimestamps();
    }
}

// End of File
