<?php

Route::prefix('v1')->group(function () {
    // Rotas para o usuário se autenticar e cadastrar
    Route::post('realizar-login', 'UserController@login');
    Route::post('cadastrar-usuario', 'UserController@register');
});

Route::prefix('v1')->middleware('auth:api')->group(function () {
    // Rotas para o Restfull de Ferramenta
    Route::post('cadastrar-ferramenta', 'FerramentaController@store');
    Route::get('listar-ferramentas', 'FerramentaController@list');
    Route::get('listar-ferramentas/{tag}', 'FerramentaController@listByTag');
    Route::delete('deletar-ferramenta/{ferramenta}', 'FerramentaController@destroy');

});

// End of File
