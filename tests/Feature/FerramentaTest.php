<?php

namespace Tests\Feature;

use App\User;
use Tests\TestCase;
use Illuminate\Foundation\Testing\DatabaseTransactions;
use Laravel\Passport\Passport;

class FerramentaTest extends TestCase
{
    use DatabaseTransactions;

    private $user;
    private $token;
    private $type;
    
    private function login()
    {
        $this->user = factory(User::class)->create();

        $response = $this->actingAs($this->user)
         ->post('/api/v1/realizar-login',[
             'email' => 'gabriel.viegas@startaideia.com.br',
             'password' => '123456'
         ]);

        $this->token = $response->baseResponse->original['access_token'];
        $this->type = $response->baseResponse->original['token_type'];
        
        $response->assertStatus(201);
        $response->assertJsonStructure([
                'access_token',
                'token_type'
            ]);
    }

    /**
     * Test para o cadastro
     * 
     */
    public function testCadastrar()
    {
        $this->login();
        
        Passport::actingAs($this->user);

        $headers = ['Authorization' => 'Bearer ' . $this->token];

        $response = $this->json('POST', '/api/v1/cadastrar-ferramenta', [
            "title" => "hotel",
            "link" => "https://github.com/typicode/hotel",
            "description" => "Local app manager. Start apps within your browser, developer tool with local .localhost domain and https out of the box.",
            "tags" => ["node", "organizing", "webapps", "domain", "developer", "https", "proxy"]
        ], [], $headers);

        $response
            ->assertStatus(201)
            ->assertJsonStructure([
                'success' => [
                    "title",
                    "link",
                    "description",
                    "updated_at",
                    "created_at",
                    "id"
                ],
            ]);
    }

     /**
      * Test para a listagem
      * 
      */
     public function testListar()
     {
        $this->login();
        
        Passport::actingAs($this->user);

        $headers = ['Authorization' => 'Bearer ' . $this->token];

        $response = $this->json('GET', '/api/v1/listar-ferramentas', [], $headers);

        $response
            ->assertStatus(201)
            ->assertJsonStructure([
                [
                    "id",
                    "title",
                    "description",
                    "link",
                    "tags"
                ]
            ]);
    }

     /**
      * Test para a listagem por tag
      * 
      */
     public function testListarPorTag()
     {
        $this->login();
        
        Passport::actingAs($this->user);

        $headers = ['Authorization' => 'Bearer ' . $this->token];

         // CUIDADO: verificar se tem o registro de id 14 na tabela tags
         $response = $this->json('GET', '/api/v1/listar-ferramentas/?tag=node', [], $headers);

        $response
            ->assertStatus(201)
            ->assertJsonStructure([
                [
                    "id",
                    "title",
                    "description",
                    "link",
                    "tags"
                ]
            ]);
    }

    /**
     * Test para exclusão
     * 
     */
    public function testExcluir()
    {
        $this->login();
        
        Passport::actingAs($this->user);

        $headers = ['Authorization' => 'Bearer ' . $this->token];

        // CUIDADO: verificar se tem o registro de id 14 na tabela ferramentas
        $response = $this->json('DELETE', '/api/v1/deletar-ferramenta/14', [], $headers);

        $response->assertStatus(204);
    }
}

// End of File
