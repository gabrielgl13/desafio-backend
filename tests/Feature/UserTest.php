<?php

namespace Tests\Feature;

use Tests\TestCase;
use Illuminate\Foundation\Testing\DatabaseTransactions;

class UserTest extends TestCase
{
    use DatabaseTransactions;

    /**
     * Testa o login do usuário
     *
     */
    public function testLogin()
    {
        $response = $this->json('POST', '/api/v1/realizar-login',[
             'email' => 'gabriel.viegas@startaideia.com.br',
             'password' => '123456'
         ]);

        $response->assertStatus(201);
        $response->assertJsonStructure([
            'access_token',
            'token_type'
        ]);
    }

    /**
     * Testa o cadastro do usuário
     * 
     */
    public function testRegister()
    {
        $response = $this->json('POST', '/api/v1/cadastrar-usuario',[
            'name' => 'Gabriel Teste',
            'email' => 'gabriel.viegas.teste@startaideia.com.br',
            'password' => '123456'
        ]);

       $response->assertStatus(201);
       $response->assertJsonStructure([
           'access_token',
           'token_type'
       ]);
    }
}
